// $(".wrap-icon-toggle").click(function () {
// 	$(this).toggleClass('active');
// 	$('body').toggleClass('overlay');
// 	$('nav#nav').toggleClass('active');
// 	$('header#header').toggleClass('active-menu');
// 	$('main#main').toggleClass('active-menu');
// 	$('footer#footer').toggleClass('active-menu');
// });
// $(document).mouseup(function (e) {
// 	var container = $("nav#nav, header#header");
// 	if (!container.is(e.target) &&
// 		container.has(e.target).length === 0) {
// 			$('.wrap-icon-toggle').removeClass('active');
// 			$('body').removeClass('overlay');
// 			$('nav#nav').removeClass('active');
// 			$('header#header').removeClass('active-menu');
// 			$('main#main').removeClass('active-menu');
// 			$('footer#footer').removeClass('active-menu');
// 	}
// });

var iconToggle = document.querySelector(".wrap-action .icon-toggle-menu")
iconToggle.addEventListener('click', function (event) {
	iconToggle.classList.toggle('active')
	document.querySelector('body').classList.toggle('overlay');
	document.getElementById('nav').classList.toggle('active');
	// $('header#header').toggleClass('active-menu');
	// $('main#main').toggleClass('active-menu');
	// $('footer#footer').toggleClass('active-menu');
})

// toggle menu child
$('.icon-arrow-menu').click(function() {
	$(this).siblings('.menu-child').slideToggle()
})

function resizeImage() {
	let arrClass = [
		//number(height / width)
	//   { class: 'resize-img-banner-main', number: (625 / 872 ) },
	  { class: 'resize-slider-building', number: (362 / 422 ) },
	  { class: 'resize', number: (1 / 1 ) },
	  { class: 'resize-register', number: (541 / 780 ) },
	  { class: 'resize-product', number: (678 / 1320 ) },
	  { class: 'resize-banner', number: (207 / 1600 ) },
	  { class: 'resize-banner-product-1', number: (338 / 648 ) },
	  { class: 'resize-video-product-2', number: (540 / 1320 ) },
	  { class: 'resize-new', number: (263 / 424 ) },
	  { class: 'resize-slide-control', number: (190 / 200 ) },
	  { class: 'resize-slide-item', number: (607 / 536 ) },
	  { class: 'resize-thumbnail-video', number: (611 / 1319) },
	  { class: 'resize-detail-1', number: (609 / 630) },
	  { class: 'resize-detail-2', number: (294 / 324) },
	  { class: 'new-small', number: (63 / 104) },
	  { class: 'resize-catalog', number: (71 / 112) },
	  { class: 'resize-bg-why', number: (231 / 1320) },
	  { class: 'resize-banner-about', number: (638 / 1603) },
	  { class: 'resize-banner-pr-2', number: (540 / 1321) },
	  { class: 'resize-banner-pr-mobi', number: (277 / 350) },
	  { class: 'resize-video-pro-detail', number: (611 / 1320) },
	  { class: 'resize-video', number: (731 / 1320) },
	  { class: 'resize-banner-showroom', number: (548 / 1320) },
	  { class: 'resize-showroom', number: (400 / 424) },
	  { class: 'resize-product-detail', number: (379 / 335) },
	  { class: 'resize-thumb', number: (190 / 200) },
	];
	arrClass.forEach(item => {
	  let elements = document.getElementsByClassName(item.class);
	  Array.from(elements).forEach(element => {
		let width = element.offsetWidth;
		element.style.height = (width * item.number) + 'px';
	  });
	});
}
function resizeBoxVideo(){
	if($("#video-youtube .wrap-video-youtube iframe").length){
		let width = $('#video-youtube .wrap-video-youtube iframe').width();
		$("#video-youtube .wrap-video-youtube iframe").css('height', width * (484 / 870) + 'px');
	}
}
$('#video-youtube').on('shown.bs.modal', function () {
	resizeBoxVideo()
  })

resizeImage();
new ResizeObserver(() => {
	resizeImage();
	resizeBoxVideo()
}).observe(document.body);



// 
var $swiperSelector = $('#slide-team-show');

$swiperSelector.each(function(index) {
    var $this = $(this);
    $this.addClass('swiper-slider-' + index);
    
    var dragSize = $this.data('drag-size') ? $this.data('drag-size') : 50;
    var freeMode = $this.data('free-mode') ? $this.data('free-mode') : false;
    var loop = $this.data('loop') ? $this.data('loop') : false;

    var swiper = new Swiper('#slide-team-show', {
      direction: 'horizontal',
      loop: loop,
      freeMode: freeMode,
      breakpoints: {
        1024: {
          slidesPerView: 3,
          spaceBetween: 24
        },
        992: {
          slidesPerView: 3,
          spaceBetween: 24
        },
        768: {
          spaceBetween: 16,
          slidesPerView: 2.5,
        },
        320: {
           slidesPerView: 1.5,
           spaceBetween: 12
        }
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      scrollbar: {
        el: '.swiper-scrollbar',
        draggable: true,
        dragSize: dragSize
      }
   });
});

// 

$('#slider-product .owl-carousel').owlCarousel({
	loop: false,
	nav: false,
	autoplay: false,
	autoWidth: true,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 24,
	responsive: {
		0: {
			items: 4,
		},
	}
});
$('.slide-product-detail').owlCarousel({
	loop: false,
	nav: false,
	autoplay: false,
	autoWidth: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 0,
	responsive: {
		0: {
			items: 1,
		},
	}
});
$('.slider-showroom').owlCarousel({
	loop: false,
	nav: false,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 24,
	responsive: {
		0: {
			items: 1.5,
		},
		
		576: {
			items: 2.5,
		},
		992: {
			items: 3,
		},

	}
});
$('.slider-banner-product-mobile').owlCarousel({
	loop: false,
	nav: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 24,
	navText: [
		`<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
		<rect x="-0.393103" y="0.393103" width="38.5241" height="38.5241" rx="19.2621" transform="matrix(-1 0 0 1 38.5243 0.310547)" stroke="#EFE4DE" stroke-width="0.786207"/>
		<path d="M25.5518 19.9653H13.7587" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M17.6899 16.0347L13.7589 19.9657L17.6899 23.8967" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>
		`,
		`<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
		<rect x="0.393103" y="-0.393103" width="38.5241" height="38.5241" rx="19.2621" transform="matrix(1 8.74228e-08 8.74228e-08 -1 3.43662e-08 38.5243)" stroke="#EFE4DE" stroke-width="0.786207"/>
		<path d="M13.7585 19.6558L25.5516 19.6558" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M21.6206 23.5864L25.5516 19.6554L21.6206 15.7244" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>
		`
	],
	responsive: {
		0: {
			items: 1.2,
			margin: 8
		},
		
		576: {
			items: 2.5,
		},
		992: {
			items: 3,
		},

	}
});
$('#slider-top-main').owlCarousel({
	loop: true,
	nav: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	lazyload: true,
	margin: 0,
	navText: [
		`<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
		<rect x="-0.393103" y="0.393103" width="38.5241" height="38.5241" rx="19.2621" transform="matrix(-1 0 0 1 38.5243 0.310547)" stroke="#EFE4DE" stroke-width="0.786207"/>
		<path d="M25.5518 19.9653H13.7587" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M17.6899 16.0347L13.7589 19.9657L17.6899 23.8967" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>
		`,
		`<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
		<rect x="0.393103" y="-0.393103" width="38.5241" height="38.5241" rx="19.2621" transform="matrix(1 8.74228e-08 8.74228e-08 -1 3.43662e-08 38.5243)" stroke="#EFE4DE" stroke-width="0.786207"/>
		<path d="M13.7585 19.6558L25.5516 19.6558" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M21.6206 23.5864L25.5516 19.6554L21.6206 15.7244" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>
		`
	],
	responsive: {
		0: {
			items: 1,
		},

	}
});

$('#custommer-about:not(.about) .owl-carousel').owlCarousel({
	loop: false,
	nav: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 24,
	navText: [
		`<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
		<rect x="-0.393103" y="0.393103" width="38.5241" height="38.5241" rx="19.2621" transform="matrix(-1 0 0 1 38.5243 0.310547)" stroke="#EFE4DE" stroke-width="0.786207"/>
		<path d="M25.5518 19.9653H13.7587" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M17.6899 16.0347L13.7589 19.9657L17.6899 23.8967" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>
		`,
		`<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
		<rect x="0.393103" y="-0.393103" width="38.5241" height="38.5241" rx="19.2621" transform="matrix(1 8.74228e-08 8.74228e-08 -1 3.43662e-08 38.5243)" stroke="#EFE4DE" stroke-width="0.786207"/>
		<path d="M13.7585 19.6558L25.5516 19.6558" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M21.6206 23.5864L25.5516 19.6554L21.6206 15.7244" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>
		`
	],
	responsive: {
		0: {
			items: 1,
		},
		768: {
			items: 2,
		},
	}
});

$('.slider-product-type').slick({
    dots: true,
    infinite: true,
	touchThreshold : 100,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
	centerMode: true,
	nextArrow: `<button class="slick-next">
	<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
		<rect x="0.393103" y="-0.393103" width="38.5241" height="38.5241" rx="19.2621" transform="matrix(1 8.74228e-08 8.74228e-08 -1 3.43662e-08 38.5243)" stroke="#EFE4DE" stroke-width="0.786207"/>
		<path d="M13.7585 19.6558L25.5516 19.6558" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M21.6206 23.5864L25.5516 19.6554L21.6206 15.7244" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		</svg></button>`,
	prevArrow: `<button class="slick-prev">
	<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
		<rect x="-0.393103" y="0.393103" width="38.5241" height="38.5241" rx="19.2621" transform="matrix(-1 0 0 1 38.5243 0.310547)" stroke="#EFE4DE" stroke-width="0.786207"/>
		<path d="M25.5518 19.9653H13.7587" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M17.6899 16.0347L13.7589 19.9657L17.6899 23.8967" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		</svg></button>`,
    responsive: [{
		 breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2.5,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 320,
            settings: {
                slidesToShow: 1.5,
                slidesToScroll: 1
            }
        }
    ]
});
$('#custommer-about.about .owl-carousel').owlCarousel({
	loop: false,
	nav: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 24,
	navText: [
		`<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
		<rect x="-0.393103" y="0.393103" width="38.5241" height="38.5241" rx="19.2621" transform="matrix(-1 0 0 1 38.5243 0.310547)" stroke="#EFE4DE" stroke-width="0.786207"/>
		<path d="M25.5518 19.9653H13.7587" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M17.6899 16.0347L13.7589 19.9657L17.6899 23.8967" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>
		`,
		`<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
		<rect x="0.393103" y="-0.393103" width="38.5241" height="38.5241" rx="19.2621" transform="matrix(1 8.74228e-08 8.74228e-08 -1 3.43662e-08 38.5243)" stroke="#EFE4DE" stroke-width="0.786207"/>
		<path d="M13.7585 19.6558L25.5516 19.6558" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M21.6206 23.5864L25.5516 19.6554L21.6206 15.7244" stroke="#EFE4DE" stroke-width="0.786207" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>
		`
	],
	responsive: {
		0: {
			items: 1,
		},
	}
});


$('#why-choose').waypoint(function () {
	$(".counter").each(function () {
		var $this = $(this),
			countTo = $this.attr("data-countto");
		countDuration = parseInt($this.attr("data-duration"));
		$({ counter: $this.text() }).animate(
			{
				counter: countTo
			},
			{
				duration: countDuration,
				easing: "linear",
				step: function () {
					$this.text(Math.floor(this.counter));
				},
				complete: function () {
					$this.text(this.counter);
				}
			}
		);
	});
}, {
	offset: '100%'
});


// fix header 
var prevScrollpos = window.pageYOffset;
window.onscroll = function () {
	var currentScrollPos = window.pageYOffset;
	if (prevScrollpos > currentScrollPos) {
		$("#header").css({
			"position": "sticky",
			"top": 0,
		});
		$("#header").addClass('active');
	} else {
		$("#header").css({
			"top": "-105px",
		});
		$("#header").removeClass('active');
	}
	prevScrollpos = currentScrollPos;
}

$(window).on('scroll', function () {
	if ($(window).scrollTop()) {
		$('#header').addClass('active');
	} else {
		$('#header').removeClass('active')
	};
});


var count = 1;
	$("#minus").click(function () {
		if (count > 1) {
			count--;
			$('#number-product').val(count);
			if($("#minus").hasClass('remove-minus')){
				$('#minus').removeClass('remove-minus')
			}
		} else {
			$('#minus').addClass('remove-minus')
		}
	});
	$("#plus").click(function () {
			count++;
			$('#number-product').val(count);
			$('#minus').removeClass('remove-minus')
	});

$('#slider-grimary-mobile').owlCarousel({
	loop: true,
	nav: false,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 8,
	responsive: {
		0: {
			items: 1.5,
		},
		
		576: {
			items: 2.5,
		},

	}
});
	
$('#slider-same-product').owlCarousel({
	loop: true,
	nav: false,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 24,
	responsive: {
		0: {
			items: 2,
		},
		768: {
			items: 3,
		},
		1080: {
			items: 4,
		},

	}
});
	
$('#slider-new-same').owlCarousel({
	loop: true,
	nav: false,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 24,
	responsive: {
		0: {
			items: 2,
		},
		768: {
			items: 2,
		},
		1080: {
			items: 3,
		},

	}
});
	
// $(document).ready(function() {
// 	$('img:not(.d-block.w-100, .not-lazy)').each(function () {
// 		$(this).addClass("lazy");
// 	});

// 	$(".box-bg-menu-mobile img").removeClass("lazy")
	  
// 	// Gọi hàm lazy load
// 	$(function () {
// 		$("img.lazy").lazyload();
// 	});

// })

// lazy
document.addEventListener("DOMContentLoaded", function() {
	var lazyloadImages;    
  
	if ("IntersectionObserver" in window) {
	  lazyloadImages = document.querySelectorAll(".lazy");
	  var imageObserver = new IntersectionObserver(function(entries, observer) {
		entries.forEach(function(entry) {
		  if (entry.isIntersecting) {
			var image = entry.target;
			image.src = image.dataset.src;
			image.classList.remove("lazy");
			imageObserver.unobserve(image);
		  }
		});
	  });
  
	  lazyloadImages.forEach(function(image) {
		imageObserver.observe(image);
	  });
	} else {  
	  var lazyloadThrottleTimeout;
	  lazyloadImages = document.querySelectorAll(".lazy");
	  
	  function lazyload () {
		if(lazyloadThrottleTimeout) {
		  clearTimeout(lazyloadThrottleTimeout);
		}    
  
		lazyloadThrottleTimeout = setTimeout(function() {
		  var scrollTop = window.pageYOffset;
		  lazyloadImages.forEach(function(img) {
			  if(img.offsetTop < (window.innerHeight + scrollTop)) {
				img.src = img.dataset.src;
				img.classList.remove('lazy');
			  }
		  });
		  if(lazyloadImages.length == 0) { 
			document.removeEventListener("scroll", lazyload);
			window.removeEventListener("resize", lazyload);
			window.removeEventListener("orientationChange", lazyload);
		  }
		}, 20);
	  }
  
	  document.addEventListener("scroll", lazyload);
	  window.addEventListener("resize", lazyload);
	  window.addEventListener("orientationChange", lazyload);
  }
})